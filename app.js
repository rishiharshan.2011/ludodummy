const express = require('express');
const googleAPI=require('./routes/loginAPI/googleAPI')
const appleAPI=require('./routes/loginAPI/appleAPI')
const facebookAPI=require('./routes/loginAPI/facebookAPI')
const userRegister=require('./routes/registerAPI/userRegister')
const userLogout=require('./routes/logoutAPI/userLogout')
const mysql = require('mysql');
const verifyToken = require('./routes/verifyToken');
const userDetails= require('./routes/userDetails');

const app= express();

var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    database:'ludo'
  });
con.connect((err)=>{
    if(err)throw err;
    else console.log('connected mysql');
})  


//routes
app.use(express.json());
app.use('/googleAPI',googleAPI);
app.use('/appleAPI',appleAPI);
app.use('/facebookAPI',facebookAPI);
app.use('/userRegister',verifyToken,userRegister);
app.use('/userLogout',verifyToken,userLogout);
app.use('/userDetails',verifyToken,userDetails);











app.listen(5000);
