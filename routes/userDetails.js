//retrieving user_name and avatar_id from the database
const express=require('express');
const jwt=require('jsonwebtoken');
const mysql = require('mysql');

var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    database:'ludo'
  });
con.connect((err)=>{
    if(err)throw err;
    else console.log('connected mysql');
})  

const router = express.Router();


router.get('/',async(req,res)=>{
    con.query(`SELECT user_name,avatar_id FROM userinfo WHERE id=${req.body.user_id}`,(err,result)=>{
        res.json(
            {
                "responseCode":200,
                "responseMessage":"Success",
                "responseData":{
                    "user_name":result[0].user_name,
                    "avatar_id":result[0].avatar_id,
                }
            }
        );
    })
})

module.exports= router;
