//middleware for verifying tokens
const jwt = require('jsonwebtoken');
const verifyToken = (req, res, next) => {
    const authHeader = req.headers.authorization;

    if (authHeader) {
        const token = authHeader.split(' ')[1]; //extracting token

        jwt.verify(token, 'yqetq3fgq', (err, user) => {
            if (err) {
                return res.json({
                    "responseCode":403,
                    "responseMessage":"Forbidden,Invalid token",
                    "responseData":{
                      
                    }
                 }
                 );
            }

            req.user = user;
            next();
        });
    } else {
        res.json({
            "responseCode":401,
            "responseMessage":"Unauthorized,Token missing",
            "responseData":{
            }
         }
         );
    }
};

module.exports = verifyToken;
